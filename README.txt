********************************************************************************************
STATUS        TESTED & WORKING
Date:         March-05-12
Tested With:  Zen Viewer 3.3.1 (1) source code compile & run (Win32).
Author:       WhiteStar Magic   

LLfunctions, OSfunctions, AAfunctions, BOTfunctions, CM/LSfunctions & Constants included.

Syntax Data & Highlighting current to:
    SecondLife  to SL-Server/12 [12.02.06.248938] *(alpha pathfinding functions not included)
    OpenSimulator               [ac934e2-r/18243]  March-05-2012
    Aurora-Sim      GIT Commit: [a2cb39b]          March-04-2012 
**********************************************************************************************
License Info : see bottom.


This is V3 viewer files related to Scripting Syntax Highlighting.
This does NOT contain the other viewer source files, only those pertinent to Syntax Highlights.

This is built on is V3 Base Source, plus the additions by Henri Beauchamps of Cool VL Viewer.
Henri's updates include the latest LL-LSL Syntax for new functions etc as deployed on LL Servers.
These are more up to date than LL Source, and other's that I have found.  Therefore I have chosen
to start with a more complete base.


=== ! DEVELOPER NOTES ! ===
This is built from V.3.3.x viewer base.  The files do NOT match up to older viewer base !
If your going to use this on older viewer source code, it will have to be cross checked & matched up.

1) These files are the only ones which as far as I can discover are the ones that require additions for OSSL.
2) Am I missing anything ?    I have asked & asked and no one is willing to share the magic incantations.
3) lllslconstants.h NOTE!  Line 214 llTextBox Magic Token exists.  llTextBox does work with this though.

\indra\lscript\lscript_library\lscript_library.cpp
\indra\lscript\lscript_http.h               // added here because of HTTP_VERBOSE_THROTTLE only
\indra\lscript\lscript_compile\indra.l       
\indra\newview\skins\default\xui\en\strings.xml  // NOTE Location & Format not the same as SG 1.5 or V.1.2 base
\indra\newview\app_settings\keywords.ini
\indra\llcommon\lllslconstants.h             // OpenSim & Aurora-Sim constants are in here


What exactly is this ?
These are only the required files as used by the in-viewer editor for highlighting the Syntax for functions.
The files are located within the same directory/folder structure as found in the Viewer Source for ease of location.
llFunctions are included. (updated revisions from Henri Beauchamps of Cool VL Viewer source)
osFunctions are included. (OSSL)
aaFunctions are included  (Aurora-Sim unique functions)
botFunctions are included (Aurora-Sim unique BOT their version of NPC functions)

Why is it this way & not a Patch ?
It's done this way because different developers are using a variety of Source Repositories.  
While I could make a GIT patch, it may or may not work on SVN, HG, etc so applying the KISS Principle,
I'll leave it up to others to plug it in as they see fit.

Why don;t I make a viewer myself ?
Simple :)  I don't have enough hair left on my head to start attacking that code pile as well...
and this is also a By-Product of some other work I am doing so it's convenient.

How is this tested ?
I've setup a Viewer Build system, thanks to a decent pinch of help from Zena Juran of Zen Viewer 
and I am using that base to build against.  

==== LICENSE INFO ====
My own work Licence:  Do What You Will with it Licence.  

Linden Labs / SecondLife:
The code in this repository is Linden Labs, SecondLife(tm) viewer 3 code and therefore falls under the appropriate license 
as specified by LL. http://wiki.secondlife.com/wiki/Linden_Lab_Official:Second_Life_Viewer_Licensing_Program

OpenSimulator / Aurora-Sim:
The information is derived from the OpenSimulator & Aurora-Sim source code which are both licensed under BSD.
Note that NONE of the source code from OpenSimulator / Aurora-Sim was used within the source code located in this repo.

OpenSimulator  http://opensimulator.org/wiki/BSD_License
Aurora-Sim     http://aurora-sim.org/

